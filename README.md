#### Using docker
* Up and run: on the project root run `docker-compose up`
* Exec into container: `docker exec -it partex_backend /bin/bash`
    * `cd app`
    * `su application`
    * `composer install`
* Import existing database using PhpMyAdmin

##### To change Magento base_url: 
`bin/magento setup:store-config:set --base-url="http://helloworld.in"`

`bin/magento cache:flush`