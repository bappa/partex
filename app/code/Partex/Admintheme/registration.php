<?php

/**
 * @author Iftakharul Alam Bappa <iftakharul@strativ.se>
 */


\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Partex_Admintheme',
    __DIR__
);